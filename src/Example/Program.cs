using System.Diagnostics;
using Petstore.Client.Api;
using Petstore.Client.Client;
using Petstore.Client.Model;

Configuration config = new Configuration();
config.BasePath = "https://petstore3.swagger.io/api/v3/";
var apiInstance = new PetApi(config);

try
{
    Pet pet = apiInstance.GetPetById(1);
    Console.WriteLine(pet);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling PetsApi.CreatePets: " + e.Message );
    Debug.Print("Status Code: "+ e.ErrorCode);
    Debug.Print(e.StackTrace);
}